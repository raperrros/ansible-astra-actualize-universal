This collection of Ansible roles configure any instance or virtual machine on Astra Linux.

Local requirements
==================

- Ansible 2.7+

- git

Remote requirements
===================

- Astra linux

Usage
=====

0. Make sure you have Ansible 2.7.

1. Get the `hosts.yml` inventory file ready and valid. It should be placed near this README.md.

2. Get main playbook file `main.yml` ready.

3. Run it all with `ansible-playbook -i hosts.yml main.yml`

HINT: Add `-l 'localhost:inventory_hostname'` key to this command if you're in hurry and wish to provision ONLY one client, instead of checking all of them
